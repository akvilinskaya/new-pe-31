// сохраняем в переменную gulp большой объект, позволяющий создавать задачи, считывать и перемещать файлы
const gulp = require("gulp");

// сохраняем в переменную concat функцию, объединяющиую все переданные ей файлы в один, и при необходимости - переименовывающую его
const concat = require("gulp-concat");

// создаем задачу на перемещение и объединение файлов - как видите, Gulp сам только ставит задачи, находит и перемещает файлы - все остальное за него делают другие модули
gulp.task("concatJS", function(){
    return gulp.src('src/js/*.js')
        .pipe(concat('main.js'))
        .pipe(gulp.dest('dist/js'));
});

gulp.task('default', (done) => {
    console.log(10, 'from gulp');
    done();
});

function moveCss () {
    return gulp.src("src/css/*.css")
        .pipe(concat("all.css"))
        .pipe(gulp.dest("dist/css"));
}

exports.build = moveCss;
