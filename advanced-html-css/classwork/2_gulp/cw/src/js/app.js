const _ = require('lodash');

const user = {
    name: 'Ivan',
    age: 22,
    family: {
        son: 'Petr',
        wife: 'Anna',
    }
}

const user3 = _.cloneDeep(user);
user3.family.son = 'Test';
console.log(user3);
console.log(user);
console.log(123)
