const gulp = require('gulp');

gulp.task('test', (done) => {
    console.log('hello from gulp');
    done();
});

gulp.task('html', () => {
    return gulp
        .src('src/index.html')
        .pipe(gulp.dest('dist'))
});

gulp.task('js', () => {
    return gulp
        .src('src/**/*.js')
        .pipe(gulp.dest('dist'))
});

gulp.task('watch', () => {
   gulp.watch(['src/index.html', 'src/**/*.js'], gulp.series('html', 'js'));
});

gulp.task('build', gulp.series('html', 'js'));

