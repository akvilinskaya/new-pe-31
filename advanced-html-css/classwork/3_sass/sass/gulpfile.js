const gulp = require('gulp');
const sass = require('gulp-sass');

const compileToCss = () => {
    return gulp
        .src('./src/sass/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('./dist/css'));
}

const html = () => {
    return gulp
        .src('./src/index.html')
        .pipe(gulp.dest('./dist'));
}
const imgs = () => {
    return gulp
        .src('./src/imgs/**')
        .pipe(gulp.dest('./dist/imgs'));
}
gulp.task('dev:css', compileToCss);
gulp.task('dev:html', html);
gulp.task('dev:img', imgs);

gulp.task('watch', () => {
   gulp.watch('./src/sass/**/*.scss', compileToCss);
   gulp.watch('./src/index.html', html);
   gulp.watch('./src/imgs/**', imgs);
});

gulp.task('build', gulp.parallel('dev:html', 'dev:css', 'dev:img'));

gulp.task('dev', gulp.series('build', 'watch'));

