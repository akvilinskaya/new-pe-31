function plant (payload = {}) {
  return {
    userName: 'John',
    secondName: 'Smith',
    age: 'immortal',
    ...payload,
  }
}

const PI = 3.14;

export {
  plant,
  PI,
}
