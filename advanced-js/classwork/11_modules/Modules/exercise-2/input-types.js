const INPUT_TYPE = Object.freeze({
    text: 'text',
    email: 'email',
    password: 'password',
    number: 'number',
    date: 'date',
    submit: 'submit',
});

export {
    INPUT_TYPE,
}
