import { INPUT_TYPE } from "./input-types.js";
import {
    sayHello,
    fetchData,
    getEventData,
} from "./listeners.js";

import {Input} from "./Input.js";

const input = new Input(
    INPUT_TYPE.number,
    'price',
    'form__item form__item--dark form__item--underlined',
    true,
    'enter product price',
    'field is required'
);

const form = document.body.querySelector('form');
form.prepend(...input.render());

input.once('focus', sayHello);
input.on('input', getEventData);
input.on('blur', fetchData);


setTimeout(() => {
    input.off('input', getEventData);
}, 3000);

