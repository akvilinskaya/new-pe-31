const sayHello = () => console.log('Hello from input');
const getEventData = (event) => console.log(event.target.value);
const fetchData = async () => {
    const p = new Promise((resolve) => setTimeout(() => {
        resolve({
            status: 'OK',
            statusCode: 200,
            message: 'All data was transferred',
            data: new Date(),
        })
    }, 5000));
    const res = await p;
    console.log(res);
};

export {
    sayHello,
    getEventData,
    fetchData,
}
