class Input {
    #wasRendered;
    constructor(type, name, classes, required, placeholder = '', errorText) {
        this.type = type;
        this.name = name;
        this.classes = classes;
        this.required = required;
        this.placeholder = placeholder;
        this.errorText = errorText;
        this.input = document.createElement('input');
        this.span = document.createElement('span');
        this.span.innerHTML = this.errorText;
        this.#wasRendered = false;
    }

    on(event, cb) {
        this.input.addEventListener(event, cb);
    }

    off(event, cb) {
        this.input.removeEventListener(event, cb);
    }

    once(event, cb) {
        this.input.addEventListener(event, cb, {once: true});
    }

    render() {
        if (this.#wasRendered) {
            return this.input;
        }

        this.input.type = this.type;
        this.input.placeholder = this.placeholder;
        this.input.required = this.required;
        this.input.name = this.name;
        this.input.className = this.classes;
        this.#wasRendered = true;
        return [this.input, this.span];
    }
}

export {
    Input,
}
