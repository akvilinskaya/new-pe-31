const express = require('express');
const cors = require('cors');
const app = express();

app.use(express.json());
app.use(cors());

app.get('/', (req, res) => {
    res.status(200).json({ name: 'john', age: 22 });
});

app.post('/', (req, res) => {
    const newUser = {
        name: req.body.user,
        age: req.body.age,
    };

    const u = DB.insert(newUser);
    u = {
        name: req.body.user,
        age: req.body.age,
        createdAt: '',
        id: '',
        updatedAt: '',
    }
    return res.status(201).set({
        'Access-Control-Allow-Origin': 'http://localhost:63342',
        'Access-Control-Allow-Method': 'GET, DELETE',
        'Access-Control-Allow-Headers': '*'
    }).json({ status: 'OK', message: 'User was added', id });
});

app.listen(3000, () => console.log('server is on 3000'))
