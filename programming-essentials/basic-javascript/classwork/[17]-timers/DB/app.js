window.onload = () => {
    const user1 = {
        name: "Ivan",
        age: 22,
        email: "ivan1999@gmail.com"
    };
    const user2 = {
        name: "Peter",
        age: 21,
        email: "peter2000@gmail.com"
    };

    localStorage.setItem(user1.email, JSON.stringify(user1));
    localStorage.setItem(user2.email, JSON.stringify(user2));

    const getAllUsersButton = document.querySelector("#gettingAllRequestBtn");
    const getOneUserButton = document.querySelector('#gettingOneRequestBtn');
    const deleteUserByEmailButton = document.querySelector('#deletingRequestBtn');
    const addUserButton = document.querySelector('#addingRequestBtn');
    // const addUserButton = document.querySelector('#addingRequestBtn');


    const userInfo = document.querySelector("#user-info");
    const userInfoList = document.querySelector("#user-info ul");
    const notificationField = document.querySelector('#user-notifications');
    const userDataInputForm = document.querySelector('#controls');


    addUserButton.addEventListener('click', () => {
        userDataInputForm.style.visibility= "visible";
    })

    getAllUsersButton.addEventListener('click', () => {
        if (!localStorage.length) {
            return addNotification('warning', "No users found")
        }
        const fragment = new DocumentFragment();
        userInfoList.innerHTML = '';
        for (const user of Object.values(localStorage)) {
            fragment.append(_userListFactory(user))
        }
        userInfoList.append(fragment);
        addNotification('success', "All found users displayed")
    });

    getOneUserButton.addEventListener('click', (event) => {
        const user = _getJSONUserFromEmail(event.target)
        if (user) {
            userInfoList.append(
                _userListFactory(user));
            const parsedUser = JSON.parse(user);
            event.target.previousElementSibling.value = '';
            addNotification('success', `User with email ${parsedUser.email} found!`)
        }
    })

    deleteUserByEmailButton.addEventListener('click', (event) => {
        const user = _getJSONUserFromEmail(event.target)
        if (user) {
            const parsedUser = JSON.parse(user);
            localStorage.removeItem(parsedUser.email)
            event.target.previousElementSibling.value = '';
            addNotification('success', `User with email ${parsedUser.email} deleted!`)
        }
    })

    const _userListFactory = (user) => {
        const userToWrite = JSON.parse(user);
        const userAsListItem = document.createElement('li');
        const x = Object.entries(userToWrite).reduce((totalStr, currentProp) => {
            return `${totalStr} ${currentProp[0]}: ${currentProp[1]} <br>`
        }, '');
        userAsListItem.insertAdjacentHTML("beforeend", x);
        return userAsListItem;
    }

    const addNotification = (type, message) => {
        notificationField.classList.add(type, "notification");
        notificationField.innerText = message;
        ClearNotification(type);
    }

    const _getJSONUserFromEmail = (button) => {
        const email = button.previousElementSibling.value;
        if (!email) {
            return addNotification('warning', "No email in input")
        }
        if (!localStorage[email]) {
            return addNotification('error', "No such user found")
        }
        return localStorage[email];
    }

    const ClearNotification = (type) => {
        setTimeout(() => {
            notificationField.classList.remove(type, "notification")
            notificationField.innerText = '';
        }, 3000)

    }


}
