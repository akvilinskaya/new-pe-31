﻿window.addEventListener('load', () => {
    const formAdditinals = document.querySelectorAll('#form2 .checkbox .food');
    const places = document.getElementById('main');
    const bonus = document.getElementById('textBonus');
    const price = document.querySelector('#price span');
    const BASE_TICKET_PRICE = 100;
    const VIP_TICKET_PRICE = 200;

    const ORDER = {
        tickets: [],
        freeItem: null,
        toggleTicket(element) {
            const buttonClasses = element.classList;
            buttonClasses.toggle('active');
            if (buttonClasses.contains('active')) {
                ORDER.addTicket(element);
            } else {
                ORDER.removeTicket(element)
            }

            this.redrawPrice()
        },
        addTicket(element) {
            const isVIP = element.classList.contains('vipBtn');
            const row = element.parentElement.dataset.row;
            this.tickets.push({
                place: element.innerText,
                row,
                price: isVIP ? VIP_TICKET_PRICE : BASE_TICKET_PRICE,
                isVIP,
                id: `${row}-${element.innerText}`,
            });

        },
        removeTicket(element) {
            const row = element.parentElement.dataset.row;
            const place = element.innerText;
            const id = `${row}-${place}`;
            this.tickets = this.tickets.filter(ticket => ticket.id !== id);
        },

        redrawPresent() {
            if (this.freeItem) {
                bonus.classList.remove('hidden');
            } else {
                bonus.classList.add('hidden');
            }
        },

        togglePresent(total) {
            if (total > 500) {
                this.freeItem = {
                    name: 'Cola + ships',
                    price: 0.01,
                }
            } else {
                this.freeItem = null;
            }
        },

        redrawPrice() {
            price.innerHTML = this.totalOrderPrice;
            this.redrawPresent();
        },

        get totalTicketsPrice() {
            return this.tickets.reduce((total, current) => total + current.price, 0);
        },

        get totalBarPrice() {
            return 0;
        },

        get totalOrderPrice() {
            const total = this.totalTicketsPrice + this.totalBarPrice;
            this.togglePresent(total);
            return total + ((this.freeItem && this.freeItem.price) || 0);
        }
    }

    const hideAdditionals = () => {
        formAdditinals.forEach(additional => {
            additional.classList.add('hidden');
        })

        bonus.classList.add('hidden');
    }

    hideAdditionals();

    places.addEventListener('click', (event) => {
        if (event.target.tagName !== 'BUTTON') {
            return;
        }

        ORDER.toggleTicket(event.target);
    });
});
