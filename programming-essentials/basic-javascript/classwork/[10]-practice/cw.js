// Задача 1
// Написать функцию, получающую на вход два числа. Если оба числа чётные - функция
// возвращает их произведение. Если оба числа нечётные - функция возвращает их сумму.
// Если одно из чисел чётное, а второе нечётное - функция возвращает это нечётное число.


// Задача 2
// Напишите функцию, которая будет принимать два аргумента (объект и свойство для удаления)
// Функция должна вернуть новый обьект (клон) без данного свойства
// * функция принимает обьект и неограниченное количество элементов для удаления


// создать функцию с двумя параметрами (object, string)
// создать новый пустой обьект
// for in - проходим по всему обьекту
// - на каждоый итерации - добавлять в новый пустой обьект ключ:значение
// - на каждоый итерации, проверить - сопадает ли ключ с переданной строкой, и если да - то не доавлять его в обьект
// возвращаем полученный после итерации обьект


// Задача 3
// напишите функцию-фабрику, которая должна вернуть объект пользователя с параметрами (name, age, yearOfBirth,
// sex, sName, country, city, address);
// по умолчанию эти данные должны быть заданы как дефолтные (любые параметры на ваш выбор)
// функция может принимать только один параметр - объект - который может содержать любые из вышеперечисленных параметров
// и менять содержимое объекта
// пример => someFunction() должна вернуть объект с параметрами (name, age, yearOfBirth, sex, sName, country, city, address); - по умолчанию
// пример => someFunction({name: 'TEST', age: 22}) должна вернуть объект с параметрами (yearOfBirth, sex, sName,
// country, city, address); - по умолчанию
// и параметрами (name, age) - которые были заданы при передаче

// cоздаем фукнцию котрая принимает один параметр - обьект (либо не принимает)
// внутри функции - нужно создать свой обьект с данными
// 1. ничего не передает => обьект со свойтвами по умолчанию
// 2. с обьектом => вернуть обьект со свойствами, где те что передал пользователь - в приоритете

// функция должна вернуть обьект с склееными свойтвами

// const createUser = (userProperties = {}) => {
//     return {
//         name: 'Anna',
//         age: 22,
//         yearOfBirth: 1990,
//         sex: 'woman',
//         sName: 'Ivanova',
//         country: 'Ukraine',
//         city: 'Kyiv',
//         address: 'Lisova',
//         ...userProperties,
//     };
//
//     return Object.assign(defaultUserProps, userProperties);
// };
//
// createUser();
// createUser();

// Задача 4
// Создать объект кофемашины, с методами
// изначально в обьекте будет 2 свойства: milk - 12, coffee - 20
// - сварить латте (2 milk, 1 coffee)
// - сварить каппучино (1 milk, 2 coffee)
// - сварить американо (3 coffee)
// - сварить эспрессо (1 coffee)
// При вызове функций - необходимо выводить в консоль: "Ваш напиток: {напиток}" и при этом отнимать то количество
// из обьекта, которое необходимо. Если на напиток не хватает - писать в консоли ошибку
// Добавить функцию - добавления продукта (кофе, молоко).
// ** сделать так, чтобы можно было вызывать функции цепочкой. Пример:
// coffeeMachine.makeLatte().makeLatte().makeEspresso()

const coffeeMachine = {
    milk: 12,
    coffee: 20,
    makeLatte() {
        if (this.milk - 2 >= 0 && this.coffee - 1 >= 0) {
            this.milk -= 2;
            this.coffee -= 1;
            console.log('Here is your latte');
        } else {
            console.error('Not enough');
        }

        return this;
    },
};

coffeeMachine
    .makeLatte()
    .makeLatte()
    .makeLatte()
// Задача 5
// Напишите функцию, которая возьмет данный обьект и пробразует из него следующее обьект:
// - name, - id, -description (если есть), - дата создания (создан N дней назад), -публичный, статус (обьект), - id
// of organisation, - currency (обьект с кодом и id), - категория тарифа,

const organisation = {
    "organisation_id": "2",
    "name": "Global Basic",
    "description": null,
    "created": "2019-03-28",
    "default_sms_mt_rate": 0.01,
    "default_sms_mo_rate": 0,
    "sim_issued_rate": 0,
    "sim_activated_rate": 0,
    "sim_suspended_rate": 0,
    "sim_activation_rate": 0,
    "sim_reactivation_rate": 0,
    "sim_suspension_rate": 0,
    "sim_termination_rate": 0,
    "public": true,
    "used_count": "202",
    "assigned_count": "180",
    "id": 582,
    "status": {"description": "ACTIVE", "id": 1},
    "currency": {"code": "EUR", "symbol": "\u20ac", "id": 1},
    "data_blocksize": {"octets": 1024, "description": "1 KB", "id": 11},
    "data_throttle": {"octets": 0, "description": "Unlimited", "id": 6},
    "tariff_category": {"name": "Global", "used_count": "7", "id": 1},
    "visibility": {"description": "public", "id": 0},
    "pdp_context_definition": []
};


// Задача 6
// написать функцию, которая получает обьект пользователя, и выводит в тело документа:
// имя, логин, id, аватар (картинкой), ссылка на гитхаб (ссылкой), компания, дату создания
const user = {
    "login": "torvalds",
    "id": 1024025,
    "node_id": "MDQ6VXNlcjEwMjQwMjU=",
    "avatar_url": "https://avatars.githubusercontent.com/u/1024025?v=4",
    "gravatar_id": "",
    "url": "https://api.github.com/users/torvalds",
    "html_url": "https://github.com/torvalds",
    "followers_url": "https://api.github.com/users/torvalds/followers",
    "following_url": "https://api.github.com/users/torvalds/following{/other_user}",
    "gists_url": "https://api.github.com/users/torvalds/gists{/gist_id}",
    "starred_url": "https://api.github.com/users/torvalds/starred{/owner}{/repo}",
    "subscriptions_url": "https://api.github.com/users/torvalds/subscriptions",
    "organizations_url": "https://api.github.com/users/torvalds/orgs",
    "repos_url": "https://api.github.com/users/torvalds/repos",
    "events_url": "https://api.github.com/users/torvalds/events{/privacy}",
    "received_events_url": "https://api.github.com/users/torvalds/received_events",
    "type": "User",
    "site_admin": false,
    "name": "Linus Torvalds",
    "company": "Linux Foundation",
    "blog": "",
    "location": "Portland, OR",
    "email": null,
    "hireable": null,
    "bio": null,
    "twitter_username": null,
    "public_repos": 6,
    "public_gists": 0,
    "followers": 131308,
    "following": 0,
    "created_at": "2011-09-03T15:26:22Z",
    "updated_at": "2021-01-10T19:36:11Z"
}

// Задача 7
// преобразовать полученное значение в массив объектов где каждый объект имеет свойтсва
//координаты - объект (latitude longitude)
//центр - объект координат
//название места
//положение - объект со значениями - адрес и категория
//отсортировать данные по параметру longitude ( в объекте коориднат )
//вывести данный массив в консоль
//использовать map/reduce/sort
const apiGeoToken = 'pk.eyJ1Ijoic2VyZ2lpcGF0b2toYSIsImEiOiJjazk0OHI1ZnUwMmxyM2hvYnh3c2o3MW0wIn0.ilbTzTis5UCw-HGlKsnQlg';
const geoURL = 'https://api.mapbox.com/geocoding/v5/mapbox.places/';
const adress = 'Kyiv';
(async () => {
    const response = await fetch(`${geoURL}${adress}.json?access_token=${apiGeoToken}&limit=20`);
    const data = await response.json();
// Your code here
    console.log(data);
// Your code here
})();
